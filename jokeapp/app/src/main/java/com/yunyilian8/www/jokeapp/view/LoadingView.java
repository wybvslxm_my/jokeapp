package com.yunyilian8.www.jokeapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import com.yunyilian8.www.jokeapp.R;


/**
 * 加载中的dialog
 */
public class LoadingView extends ProgressDialog {

    public LoadingView(Context context) {
        super(context, R.style.cart_dialog);
    }

    Animation operatingAnim;
    View mouse;

    private void init(){
        operatingAnim = new RotateAnimation( 0f,360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        operatingAnim.setRepeatCount(Animation.INFINITE);
        operatingAnim.setDuration(2000);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        mouse = findViewById(R.id.mouse);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loadview);
        init();
    }

    @Override
    public void show() {
        super.show();
        mouse.setAnimation(operatingAnim);
    }

    @Override
    public void dismiss() {
        operatingAnim.reset();
        mouse.clearAnimation();
        super.dismiss();
    }
}
