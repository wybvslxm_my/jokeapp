package com.yunyilian8.www.jokeapp.bean;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/23  18:59
 * description:
 *****************************************************/
public class AnyEventType {
    public int mType;
    public Object mObj;

    public AnyEventType(int type,Object obj) {
        mType = type;
        mObj = obj;
    }
    public int getType(){
        return mType;
    }
    public Object getObj(){
        return mObj;
    }
}
