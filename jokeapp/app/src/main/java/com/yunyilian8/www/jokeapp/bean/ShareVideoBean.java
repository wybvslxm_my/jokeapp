package com.yunyilian8.www.jokeapp.bean;

import java.io.Serializable;
import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/27  14:05
 * description:
 *****************************************************/
public class ShareVideoBean implements Serializable {

    public List<VideoFileNameBean> list;

    public List<VideoFileNameBean> getList() {
        return list;
    }
    public void setList(List<VideoFileNameBean> list) {
        this.list = list;
    }

}
