package com.yunyilian8.www.jokeapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.base.SimpleBaseAdapter;
import com.yunyilian8.www.jokeapp.bean.SmilingFaceTextBean;
import com.yunyilian8.www.jokeapp.utils.DateTimeUtil;

import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/13  10:07
 * description: 笑话列表对应的adapter
 *****************************************************/
public class SmilingFaceTextAdapter extends SimpleBaseAdapter<SmilingFaceTextBean> {
    public SmilingFaceTextAdapter(Context context) {
        super(context);
    }

    public SmilingFaceTextAdapter(Context context, List list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyHolder holder = null;
        if (null == convertView)
        {
            convertView = View.inflate(mContext, R.layout.item_smiling_face_text, null);
            holder = new MyHolder();
            holder.mContent = (TextView) convertView.findViewById(R.id.tv_content);
            holder.mTime = (TextView) convertView.findViewById(R.id.tv_time);

            convertView.setTag(holder);
        }else {
            holder = (MyHolder) convertView.getTag();
        }

        SmilingFaceTextBean bean = mList.get(position);
        holder.mContent.setText(splitStringData(bean.getContent()));
        holder.mTime.setText(DateTimeUtil.formatDataToYMD(bean.getUnixtime()));

        return convertView;
    }

    private String splitStringData(String str){

        String[] split = str.split("　　");

        if (split.length == 0)
        {
            String s = str.replace("#", "");
            split = s.split("");
        }

        String s = "";
        for (int i = 0; i < split.length; i ++){
            if (i == split.length - 1)
            {
                s += split[i];
            }else {
                s += split[i] + "\r\n";
            }
        }

        return s;
    }


    class MyHolder{
        TextView mContent;
        TextView mTime;
    }
}
