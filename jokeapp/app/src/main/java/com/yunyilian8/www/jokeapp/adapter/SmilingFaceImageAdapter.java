package com.yunyilian8.www.jokeapp.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.base.SimpleBaseAdapter;
import com.yunyilian8.www.jokeapp.bean.SmilingFaceImageBean;
import com.yunyilian8.www.jokeapp.utils.DateTimeUtil;

import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/13  15:30
 * description:
 *****************************************************/
public class SmilingFaceImageAdapter extends SimpleBaseAdapter<SmilingFaceImageBean> {
    public SmilingFaceImageAdapter(Context context) {
        super(context);
    }

    public SmilingFaceImageAdapter(Context context, List list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder holder = null;
        if (null == convertView){
            convertView = View.inflate(mContext, R.layout.item_smiling_face_image,null);
            holder = new MyHolder();
            holder.mIvImage = (GifImageView) convertView.findViewById(R.id.iv_image);
            holder.mTvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.mTvTime = (TextView) convertView.findViewById(R.id.tv_time);

            convertView.setTag(holder);
        }else {
            holder = (MyHolder) convertView.getTag();
        }

        SmilingFaceImageBean bean = mList.get(position);
       //ImageLoader.displayImageGIF(mContext,holder.mIvImage,bean.getUrl());
       Glide.with(mContext).load(bean.getUrl()).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.mIvImage);


        holder.mIvImage.setImageURI(Uri.parse(bean.getUrl()));
        holder.mTvName.setText(bean.getContent());
        holder.mTvTime.setText(DateTimeUtil.formatDataToYMD(bean.getUnixtime()));

        return convertView;
    }
    class MyHolder{
        GifImageView mIvImage;
        TextView mTvName;
        TextView mTvTime;
    }

}
