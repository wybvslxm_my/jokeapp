package com.yunyilian8.www.jokeapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.base.SimpleBaseAdapter;
import com.yunyilian8.www.jokeapp.bean.DateHistoricalListBean;

import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/3/3  10:37
 * description: 历史上的今天list对应adapter
 *****************************************************/
public class DateHistoricalAdapter extends SimpleBaseAdapter<DateHistoricalListBean> {
    public DateHistoricalAdapter(Context context) {
        super(context);
    }

    public DateHistoricalAdapter(Context context, List list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder holder = null;
        if (null == convertView){
            holder = new MyHolder();
            convertView = View.inflate(mContext, R.layout.item_date_historical, null);
            holder.mTvYear = (TextView) convertView.findViewById(R.id.tv_year);
            holder.mTvDate = (TextView) convertView.findViewById(R.id.tv_date);
            holder.mTvContent = (TextView) convertView.findViewById(R.id.tv_content);
            convertView.setTag(holder);
        }else {
            holder = (MyHolder) convertView.getTag();
        }
        DateHistoricalListBean bean = mList.get(position);
        holder.mTvYear.setText(bean.getDate().split("年")[0]);
        holder.mTvDate.setText(bean.getDate().split("年")[1]);
        holder.mTvContent.setText(bean.getTitle());

        return convertView;
    }
    class MyHolder{
        private TextView mTvYear;
        private TextView mTvDate;
        private TextView mTvContent;
    }
}
