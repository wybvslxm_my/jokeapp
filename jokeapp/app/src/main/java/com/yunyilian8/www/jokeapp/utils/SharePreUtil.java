package com.yunyilian8.www.jokeapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.yunyilian8.www.jokeapp.bean.ShareVideoBean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import static android.content.Context.MODE_PRIVATE;


public class SharePreUtil {


	private final static String FILENAME = "login_data_save";

	// ====================StringPreference========================
	/**
	 * 设置StringPreference
	 * 
	 * @param context
	 * @param key
	 * @param content
	 */
	public static void setString(Context context, String key, String content) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, content);
		editor.commit();
	}

	/**
	 * 获取StringPreference
	 * 
	 * @param context
	 * @param key
	 * @param defaultStr
	 * @return
	 */
	public static String getString(Context context, String key, String defaultStr) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getString(key, defaultStr);
	}

	// ====================IntegerPreference========================
	/**
	 * 设置IntPreference
	 * 
	 * @param context
	 * @param key
	 * @param content
	 */
	public static void setInt(Context context, String key, int content) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(key, content);
		editor.commit();
	}

	/**
	 * 获取IntPreference
	 * 
	 * @param context
	 * @param key
	 * @return
	 */
	public static int getInt(Context context, String key) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getInt(key, -1);
	}

	// ====================BooleanPreference========================
	/**
	 * 设置BooleanPreference
	 * 
	 * @param context
	 * @param key
	 * @param content
	 */
	public static void setBoolean(Context context, String key, boolean content) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(key, content);
		editor.commit();
	}

	/**
	 * 获取BooleanPreference
	 * 
	 * @param context
	 * @param key
	 * @param defaultBoolean
	 * @return
	 */
	public static boolean getBoolean(Context context, String key, boolean defaultBoolean) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getBoolean(key, defaultBoolean);
	}

	// ====================FloatPreference========================
	/**
	 * 设置FloatPreference
	 * 
	 * @param context
	 * @param key
	 * @param content
	 */
	public static void setFloat(Context context, String key, float content) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putFloat(key, content);
		editor.commit();
	}

	/**
	 * 获取FloatPreference
	 * 
	 * @param context
	 * @param key
	 * @param defaultFloat
	 * @return
	 */
	public static float getFloat(Context context, String key, float defaultFloat) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getFloat(key, defaultFloat);
	}

	// ====================LongPreference========================
	/**
	 * 设置LongPreference
	 * 
	 * @param context
	 * @param key
	 * @param content
	 */
	public static void setLong(Context context, String key, long content) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putLong(key, content);
		editor.commit();
	}

	/**
	 * 获取LongPreference
	 * 
	 * @param context
	 * @param key
	 * @param defaultLong
	 * @return
	 */
	public static long getLong(Context context, String key, long defaultLong) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getLong(key, defaultLong);
	}


	/**
	 *sp中存入list数据
	 * @param context
	 * @param key
	 * @param bean
     */
	public static void setList(Context context,String key,ShareVideoBean bean) {
		SharedPreferences preferences = context.getSharedPreferences("base64",
				MODE_PRIVATE);
		// 创建字节输出流
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			// 创建对象输出流，并封装字节流
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			// 将对象写入字节流
			oos.writeObject(bean);
			// 将字节流编码成base64的字符窜
			/*String oAuth_Base64 = new String(Base64.encodeBase64(baos
					.toByteArray()));*/
			String oAuth_Base64 = Base64.encodeToString(baos.toByteArray(), 1);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString(key, oAuth_Base64);

			editor.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * sp中取出sp数据
	 * @param context
	 * @param key
	 * @param def
     * @return
     */
	public static ShareVideoBean getList(Context context,String key,String def) {
		ShareVideoBean bean = null;
		SharedPreferences preferences = context.getSharedPreferences("base64",
				MODE_PRIVATE);
		String productBase64 = preferences.getString(key, def);

		//读取字节
	//	byte[] base64 = Base64.decodeBase64(productBase64.getBytes());
		byte[] base64 = Base64.encode(productBase64.getBytes(),1);

		//封装到字节流
		ByteArrayInputStream bais = new ByteArrayInputStream(base64);
		try {
			//再次封装
			ObjectInputStream bis = new ObjectInputStream(bais);
			try {
				//读取对象
				bean = (ShareVideoBean) bis.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bean;
	}


	public static void setObject(Context context,String key,ShareVideoBean shareVideoBean){
		ToastUtil.showToast(context,shareVideoBean+"");
		try {
			setString(context,key, serialize(shareVideoBean));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ShareVideoBean getObject(Context context,String key){
		try {
			ShareVideoBean bean = deSerialization(getString(context, key, ""));
			return bean;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}



	/**
	 * 序列化对象
	 *
	 * @param bean
	 * @return
	 * @throws IOException
	 */
	private static String serialize(ShareVideoBean bean) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				byteArrayOutputStream);
		objectOutputStream.writeObject(bean);
		String serStr = byteArrayOutputStream.toString("ISO-8859-1");
		serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
		objectOutputStream.close();
		byteArrayOutputStream.close();
		Log.d("serial", "serialize str =" + serStr);

		return serStr;
	}

	/**
	 * 反序列化对象
	 *
	 * @param str
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static ShareVideoBean deSerialization(String str) throws IOException,
			ClassNotFoundException {

		String redStr = java.net.URLDecoder.decode(str, "UTF-8");
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
				redStr.getBytes("ISO-8859-1"));
		ObjectInputStream objectInputStream = new ObjectInputStream(
				byteArrayInputStream);
		ShareVideoBean bean = (ShareVideoBean) objectInputStream.readObject();
		objectInputStream.close();
		byteArrayInputStream.close();

		return bean;
	}

}
