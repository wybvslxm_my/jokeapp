package com.yunyilian8.www.jokeapp.bean;

import java.io.Serializable;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/23  12:00
 * description: assets文件名数据封装bean
 *****************************************************/
public class VideoFileNameBean implements Serializable {
    private String name;
    private boolean unfolded;
    private String url;
    private boolean collect;
    private boolean isAssetsFile;


    public boolean isAssetsFile() {
        return isAssetsFile;
    }

    public void setAssetsFile(boolean assetsFile) {
        isAssetsFile = assetsFile;
    }

    public boolean isCollect() {
        return collect;
    }

    public void setCollect(boolean collect) {
        this.collect = collect;
    }

    public String getURL() {
        return url;
    }

    public void setURL(String filePath) {
        this.url = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isUnfolded() {
        return unfolded;
    }

    public void setUnfolded(boolean unfolded) {
        this.unfolded = unfolded;
    }
}
