package com.yunyilian8.www.jokeapp.fragment;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.adapter.CustomViewPagerAdapter;
import com.yunyilian8.www.jokeapp.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/10  14:12
 * description: 笑话主fragment
 *****************************************************/
public class HomeSmilingFaceFragment extends BaseFragment {

    @InjectView(R.id.tv_01)
    TextView mTv01;
    @InjectView(R.id.tv_02)
    TextView mTv02;

    @InjectView(R.id.vp_viewPager)
    ViewPager mViewPager;
    @InjectView(R.id.iv_tab_bg)
    ImageView mIvTabBg;
    private View mView;
    int offset = 0;         // 偏移量
    int currIndex = 0;      // 当前页卡编号
    int bmpW;               // 图片宽度
    /**
     * fragment管理器
     **/
    private FragmentManager mFragmentManager;
    /**
     * 用于填充viewPager的list数据
     **/
    private List<Fragment> mFragmentList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_home_smiling_face, null);
        ButterKnife.inject(this, mView);

        initViewPager();
        initImageView();
        initListener();

        return mView;
    }

    /**
     * 设置监听.
     */
    private void initListener() {

        //设置viewPager的监听
        /**
         * 页面1到页面2的偏移量
         **/
        final int one = offset * 2 + bmpW;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                /**初始化移动的动画（从当前位置，x平移到即将要到的位置）**/
                Animation animation = new TranslateAnimation(currIndex * one, position
                        * one, 0, 0);
                currIndex = position;
                /**动画终止时停留在最后一帧，不然会回到没有执行前的状态**/
                animation.setFillAfter(true);
                /**动画持续时间，0.2秒**/
                animation.setDuration(200);
                /**是用imageView来显示动画**/
                mIvTabBg.startAnimation(animation);
                /**设置文本颜色**/
                setTextColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * 设置文本颜色
     *
     * @param position
     */
    public void setTextColor(int position) {
        switch (position) {
            case 0:
                /**推荐**/
                mTv01.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTv02.setTextColor(getActivity().getResources().getColor(R.color.gray20));
                break;
            case 1:
                /**分类**/
                mTv02.setTextColor(getActivity().getResources().getColor(R.color.white));
                mTv01.setTextColor(getActivity().getResources().getColor(R.color.gray20));
                break;
        }
    }

    /**
     * 初始化viewPager
     */
    private void initViewPager() {
        try {
            mFragmentManager = getActivity().getSupportFragmentManager();

            /**'文字笑话'**/
            mFragmentList.add(new SmilingFaceTextFragment());

            /**'趣图'**/
            mFragmentList.add(new SmilingFaceImageFragment());

            // mViewPager.setOnPageChangeListener(this);
            mViewPager.setAdapter(new CustomViewPagerAdapter(mFragmentList, mFragmentManager));
            mViewPager.setCurrentItem(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    @OnClick({R.id.tv_01, R.id.tv_02})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_01:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.tv_02:
                mViewPager.setCurrentItem(1);
                break;

        }
    }

    /**
     * 初始化动画图片
     */
    private void initImageView() {
        bmpW = BitmapFactory.decodeResource(getResources(), R.mipmap.viewpager_table_bg)
                .getWidth();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        /**获取手机屏幕宽度分辨率**/
        int screenW = dm.widthPixels;
        /**获取图片偏移量**/
        offset = (screenW / 4                                                                                                                                                                                                         - bmpW) / 2;
        /** imageView设置平移，使下划线平移到初始位置（平移一个offset）**/
        Matrix matrix = new Matrix();
        matrix.postTranslate(offset, 0);
        mIvTabBg.setImageMatrix(matrix);
    }
}
