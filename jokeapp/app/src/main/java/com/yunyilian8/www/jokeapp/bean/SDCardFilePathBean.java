package com.yunyilian8.www.jokeapp.bean;

import java.io.Serializable;
import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/3/1  15:48
 * description: 用户自己添加的文件对象 封装
 *****************************************************/
public class SDCardFilePathBean implements Serializable {
    private List<FileNameBean> mList;

    public List<FileNameBean> getmList() {
        return mList;
    }

    public void setmList(List<FileNameBean> mList) {
        this.mList = mList;
    }
}
