package com.yunyilian8.www.jokeapp.base;//package com.sz.qjt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.view.LoadingView;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * FragmentActivity的基础类
 */
public class BaseFragmentActivity extends FragmentActivity {

    public Context mCtx;
    protected LoadingView loadingView;
    //	protected UserSetFragment mFrag;
//	private PushMsgDialog mDialog;
    protected CompositeSubscription mCompositeSubscription;
    public Context mAppContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = this;
        mAppContext = mCtx.getApplicationContext();
        mCompositeSubscription = new CompositeSubscription();
        loadingView = new LoadingView(mCtx);
	//	EventBus.getDefault().register(this);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
    }
    /**
     * 关闭当前界面,进入新的activity,解决退出和进入activity动画冲突的问题
     */
    public void finishToNewActivity() {
        super.finish();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.push_right_in,R.anim.push_left_out);
    }

    protected void loadDataFromNet(Subscriber subscriber, Observable observable) {
        mCompositeSubscription.add(
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(subscriber)
        );
    }


    protected void showLoadingView() {
        if (!loadingView.isShowing())
            loadingView.show();
    }

    protected void hideLoadingView() {
        if (loadingView.isShowing()) {
            loadingView.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
   //     EventBus.getDefault().unregister(this);

    }

    /**
     public void onEventMainThread(AnyEventType event) {
     if (event.getType() == 2001) {
     mSlidingMenu.toggle();
     } else if (event.getType() == 2222) {
     List<PushMsg> msgList = (List<PushMsg>) event.mObj;
     if(mDialog == null){
     mDialog = new PushMsgDialog(this, R.style.CustomDialogStyle);
     mDialog.show();
     mDialog.setData(msgList);
     }else{
     if(mDialog.isShowing()){
     mDialog.updateData(msgList);
     }else{
     mDialog = new PushMsgDialog(this, R.style.CustomDialogStyle);
     mDialog.show();
     mDialog.setData(msgList);
     }
     }
     }
     }
     */

}
