package com.yunyilian8.www.jokeapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/13  10:32
 * description:
 *****************************************************/
public class SmilingFaceTextBean implements Parcelable {

    /**
     * content : 唐僧师徒四人坐在草地上休息，两个小妖远远的观望着。　　
     * 甲对乙说：“大王叫我们抓唐僧，却不知这四个哪个才是？”　　
     * 乙道：“我也不认识，不过听说唐僧是这四人的领导，就跟咱大王一样！”　　
     * 甲松了口气：“你早说嘛，领导的特征太明显了！”　　
     * 于是……猪八戒被抓走了。
     * hashId : a064e70878090979a39b2949983b00e4
     * unixtime : 1486946630
     * updatetime : 2017-02-13 08:43:50
     */

    private String content;
    private String hashId;
    private int unixtime;
    private String updatetime;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public int getUnixtime() {
        return unixtime;
    }

    public void setUnixtime(int unixtime) {
        this.unixtime = unixtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.content);
        dest.writeString(this.hashId);
        dest.writeInt(this.unixtime);
        dest.writeString(this.updatetime);
    }

    public SmilingFaceTextBean() {
    }

    protected SmilingFaceTextBean(Parcel in) {
        this.content = in.readString();
        this.hashId = in.readString();
        this.unixtime = in.readInt();
        this.updatetime = in.readString();
    }

    public static final Parcelable.Creator<SmilingFaceTextBean> CREATOR = new Parcelable.Creator<SmilingFaceTextBean>() {
        @Override
        public SmilingFaceTextBean createFromParcel(Parcel source) {
            return new SmilingFaceTextBean(source);
        }

        @Override
        public SmilingFaceTextBean[] newArray(int size) {
            return new SmilingFaceTextBean[size];
        }
    };
}
