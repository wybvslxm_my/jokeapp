package com.yunyilian8.www.jokeapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.base.BaseFragment;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/10  14:13
 * description:
 *****************************************************/
public class HomeNull03Fragment extends BaseFragment {
    private View mView;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_home_null03, null);
        return mView;
    }

}
