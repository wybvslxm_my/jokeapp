package com.yunyilian8.www.jokeapp.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.yunyilian8.www.jokeapp.R;

import java.io.File;

import static com.yunyilian8.www.jokeapp.utils.SDCardUtils.hasSdcard;

/**
 * 选择头像
 */
public class PhotoChooserDialog extends Dialog {

    private Activity mContext = null;
    private Window window = null;
    /**
     * 拍照
     */
    private static final int PHOTO_REQUEST_CAREMA = 1;
    /**
     * 从相册中选择
     **/
    private static final int PHOTO_REQUEST_GALLERY = 2;
    /**
     * 结果
     **/
    private static final int PHOTO_REQUEST_CUT = 3;
    private File tempFile;
    /**
     * 头像名称
     **/
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";

    public PhotoChooserDialog(Activity context) {
        super(context, R.style.choose_photo_dialog);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.setContentView(R.layout.layout_dialog_choose_photo);
            Window window = getWindow();
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.y = 20;
            window.setAttributes(lp);
            window.setGravity(Gravity.BOTTOM);
            this.setCancelable(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialog() {
        /**dialog弹出动画**/
        windowDeploy(0, 0);
        this.show();
        this.findViewById(R.id.tv_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        this.findViewById(R.id.tv_byLocal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                dismiss();
            }
        });
        this.findViewById(R.id.tv_byTakePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
                dismiss();
            }
        });
    }

    /**
     * 窗体弹出动画
     *
     * @param x
     * @param y
     */
    public void windowDeploy(int x, int y) {
        /**得到对话框**/
        window = getWindow();
        /**设置窗口弹出动画**/
        window.setWindowAnimations(R.style.dialogWindowAnim);
        /**设置对话框背景为透明**/
        window.setBackgroundDrawableResource(R.color.transparent);
        WindowManager.LayoutParams wl = window.getAttributes();

        /**根据x，y坐标设置窗口需要显示的位置**/
        wl.x = x; //x小于0左移，大于0右移
        wl.y = y; //y小于0上移，大于0下移

//      wl.alpha = 0.6f; //设置透明度
        wl.gravity = Gravity.AXIS_CLIP; //设置重力
        window.setAttributes(wl);
    }

    /**
     * 打开相册
     */
    private void openGallery() {
        /**激活系统图库，选择一张图片**/
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        /**开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY**/
        mContext.startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
    }

    /**
     * 打开相机
     */
    private void openCamera() {
        /**激活相机**/
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        /** 判断存储卡是否可以用，可用进行存储**/
        if (hasSdcard()) {
            tempFile = new File(Environment.getExternalStorageDirectory(),
                    PHOTO_FILE_NAME);
            /** 从文件中创建uri**/
            Uri uri = Uri.fromFile(tempFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        /**开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CAREMA**/
        mContext.startActivityForResult(intent, PHOTO_REQUEST_CAREMA);
    }

    /**
     * 剪切图片
     */
    public void crop(Uri uri) {
        /**裁剪图片意图**/
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        /** 裁剪框的比例，1：1**/
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        /**裁剪后输出图片的尺寸大小**/
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);
        /**图片格式**/
        intent.putExtra("outputFormat", "JPEG");
        /**取消人脸识别**/
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", true);
        /**开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CUT**/
        mContext.startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }
    public File getTempFile()
    {
        return tempFile;
    }


}
