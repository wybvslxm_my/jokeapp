package com.yunyilian8.www.jokeapp.utils;

import android.os.Environment;

/**
 * 文件缓存目录 工具类
 * Created by Administrator on 2016/5/3.
 */
public class Contants {

    public static String getRootPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    public static final String RootPath = getRootPath() + "/serviceAPP/";
    /**收藏直播列表缓存目录**/
    public static final String RadioCollect = RootPath + "ser/Radio_Collect.ini";
    /**用户自己添加的数据缓存目录**/
    public static final String SDCardPathBean = RootPath + "ser/SDCard_Path_Bean.ini";

    /**缓存的文件路径**/
    public static final String mp4CachePath = RootPath + "mp4/";
    public static final String docCachePath = RootPath + "doc/";
    public static final String pdfCachePath = RootPath + "pdf/";
    public static final String DownloadPath = RootPath + "download/";

    /**app崩溃log收集的文件夹地址**/
    public static final String ExceptionLogPath = RootPath + "exception/";

}
