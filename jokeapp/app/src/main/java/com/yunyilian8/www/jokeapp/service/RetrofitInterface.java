package com.yunyilian8.www.jokeapp.service;

import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * retrofit网络请求接口
 * 包含
 * 1.请求的方式
 * 2.请求的路径
 * 3.请求的参数
 * 类说明
 */
public interface RetrofitInterface {


    /** 获取文本笑话 */
    @POST("joke/content/text.from?")
    Observable<String> getSmilingFaceTextData(@Query("key") String name, @Query("page") int page, @Query("pagesize") int pageSize) ;


    /** 获取图片笑话 */
    @POST("joke/img/text.from?")
    Observable<String> getSmilingFaceImageData(@Query("key") String name, @Query("page") int page, @Query("pagesize") int pageSize) ;

    /** 历史上的今天 */
    @POST("http://v.juhe.cn/todayOnhistory/queryEvent.php?")
    Observable<String> queryEvent(@Query("date") String date,@Query("key") String key);
}
