package com.yunyilian8.www.jokeapp.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.rollviewpager.OnItemClickListener;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.adapter.DateHistoricalAdapter;
import com.yunyilian8.www.jokeapp.adapter.TestNomalAdapter;
import com.yunyilian8.www.jokeapp.base.BaseActivity;
import com.yunyilian8.www.jokeapp.bean.DateHistoricalListBean;
import com.yunyilian8.www.jokeapp.service.RetrofitService;
import com.yunyilian8.www.jokeapp.utils.AnimationUtils;
import com.yunyilian8.www.jokeapp.utils.DateTimeUtil;
import com.yunyilian8.www.jokeapp.utils.JSONUtils;
import com.yunyilian8.www.jokeapp.utils.ToastUtil;
import com.yunyilian8.www.jokeapp.view.RollPagerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Subscriber;

/**
 * 程序入口activity 用户首次进来,进入到该界面
 */
public class MainActivity extends BaseActivity {

    //轮播图
    @InjectView(R.id.rpv_page)
    RollPagerView mRpvPage;
    //文字头
    @InjectView(R.id.tv_title)
    TextView mTvTitle;
    //文字内容
    @InjectView(R.id.tv_content)
    TextView mTvContent;

    //进入应用按钮
    @InjectView(R.id.tv_next)
    TextView mTvNext;
    @InjectView(R.id.lv_content)
    ListView mListView;
    @InjectView(R.id.fl_layout_lodding)
    RelativeLayout mRlLodding;

    private TestNomalAdapter mNormalAdapter;
    private String[] mTitleString;
    private String[] mContentString;
    private List<DateHistoricalListBean> mList = new ArrayList<>();
    private int mMonth;
    private int mDay;
    private String mVersion;
    private DateHistoricalAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        /**设置透明状态栏**/
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        initListView();
        initData(false);

        //初始化viewPager
        initViewPager();

        //监听
        initListener();
    }

    private void initListView() {
        mAdapter = new DateHistoricalAdapter(mCtx, mList);
        mListView.setAdapter(mAdapter);

    }

    private void initData(boolean showDialog) {

        loadTodayData(false);

        //设置数据
        mTitleString = new String[]{"新一天",
                "感谢你",
                "一天",
                "人生"};
        mContentString = new String[]{"每天都是新的一天,我们要加油!",
                "感谢一路有你的陪伴让我感觉很好",
                "日行一善,勤行一生.灿烂人生,幸福人生!",
                "每个人都要为自己的人生负全责,我们不能将责任推给任何人,任何事!"};

        mVersion = "1.0";
        mMonth = DateTimeUtil.getMonth();
        mDay = DateTimeUtil.getDay();
    }

    /**
     * 从网络加载数据
     *
     * @param showDialog
     */
    private void loadTodayData(boolean showDialog) {
        int month = DateTimeUtil.getMonth();
        int day = DateTimeUtil.getDay();
        final String date = month + "/" + day;

        loadDataFromNet(new Subscriber<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable throwable) {
                ToastUtil.showToast(mAppContext, "获取数据失败！");
                mRlLodding.setVisibility(View.GONE);
            }

            @Override
            public void onNext(String result) {

                try {
                    if (result != null) {
                        JSONObject jsonObject = JSONUtils.getJSONObject(result);
                        String dataArray = jsonObject.getString("result");
                        setDate(dataArray);
                        mRlLodding.setVisibility(View.GONE);
                    } else {
                        ToastUtil.showToast(mAppContext, "获取数据失败");
                        mRlLodding.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    ToastUtil.showToast(mAppContext, "获取数据失败");
                    mRlLodding.setVisibility(View.GONE);
                }
            }
        }, RetrofitService.getInstance().queryEvent(date));
        if (showDialog)
            showLoadingView();
    }

    /**
     * 点击事件
     */
    private void initListener() {

        //按钮的点击监听
        mTvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnimationUtils.setScaleAnimation01(mTvNext, 1f, 0f, 200, true);
                AnimationUtils.setListener(new AnimationUtils.ReduceAnimationListener() {
                    @Override
                    public void isEnd() {
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intent);

                        finishToNewActivity();
                    }
                });
            }
        });

        //viewPager的点击监听
        mRpvPage.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(getApplicationContext(), "Item " + position + " clicked", Toast.LENGTH_SHORT).show();
            }
        });

        //viewPager的页面滚动监听
        mRpvPage.setOnPageChangeListener(new RollPagerView.PageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int position) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageSelected(int position) {
                //设置文字标题
                mTvTitle.setText(mTitleString[position]);
                AnimationUtils.rightToLeftShow(mTvTitle);

                //设置文字内容
                mTvContent.setText(mContentString[position]);
                AnimationUtils.setScaleAnimation01(mTvContent, 1.1f, 1f, 500, false);
            }
        });
    }

    /**
     * 初始化viewPager
     */
    private void initViewPager() {
        /**设置顶部viewPager获得焦点,防止scrollView不在顶部的问题**/
        mRpvPage.setFocusable(true);
        mRpvPage.setFocusableInTouchMode(true);
        mRpvPage.requestFocus();


        //默认textView的初始数据
        mTvTitle.setText(mTitleString[0]);
        mTvContent.setText(mContentString[0]);
        mRpvPage.setPlayDelay(3000);
        mRpvPage.setAdapter(mNormalAdapter = new TestNomalAdapter(this, mContentString));
        mRpvPage.setHintView(new ColorPointHintView(this, Color.YELLOW, Color.WHITE));

    }

    /**
     * 设置list数据
     *
     * @param data
     */
    public void setDate(String data) {
        mList = JSONUtils.getList(data, DateHistoricalListBean.class);
        mAdapter.setList(mList);
        mAdapter.notifyDataSetChanged();
    }
}
