package com.yunyilian8.www.jokeapp.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.activity.HomeActivity;
import com.yunyilian8.www.jokeapp.base.BaseFragment;
import com.yunyilian8.www.jokeapp.listener.PhoChooerDialogListener;
import com.yunyilian8.www.jokeapp.utils.ToastUtil;
import com.yunyilian8.www.jokeapp.view.SettingItemLayout;
import com.yunyilian8.www.jokeapp.view.XCRoundImageView;
import com.yunyilian8.www.jokeapp.view.dialog.PhotoChooserDialog;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import static com.yunyilian8.www.jokeapp.utils.SDCardUtils.hasSdcard;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/10  14:14
 * description: 我的界面
 *****************************************************/
public class HomeMyFragment extends BaseFragment {
    @InjectView(R.id.iv_title)
    XCRoundImageView mIvTitle;
    @InjectView(R.id.ll_not_login)
    LinearLayout mLlNotLogin;
    @InjectView(R.id.tv_name)
    TextView mTvName;
    @InjectView(R.id.tv_phone)
    TextView mTvPhone;
    @InjectView(R.id.ll_login)
    LinearLayout mLlLogin;
    @InjectView(R.id.sil_01)
    SettingItemLayout mSil01;
    @InjectView(R.id.sil_02)
    SettingItemLayout mSil02;
    @InjectView(R.id.sil_03)
    SettingItemLayout mSil03;
    @InjectView(R.id.sil_04)
    SettingItemLayout mSil04;
    @InjectView(R.id.sil_05)
    SettingItemLayout mSil05;
    private View mView;
    private PhotoChooserDialog mPhoChooerDialog;
    private File tempFile;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_home_my, null);
        ButterKnife.inject(this, mView);

        initView();
        return mView;
    }


    private void initView() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick({R.id.iv_title, R.id.ll_not_login, R.id.sil_01, R.id.sil_02, R.id.sil_03, R.id.sil_04, R.id.sil_05})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_title:
                //点击了头部选中头像
                setUserHead();
                break;
            case R.id.ll_not_login:
                //登录注册按钮布局
                break;
            case R.id.sil_01:
                //我的购买
                break;
            case R.id.sil_02:
                //我的红包
                break;
            case R.id.sil_03:
                //1我的收藏
                break;
            case R.id.sil_04:
                //设置
                break;
            case R.id.sil_05:
                //关于
                break;
        }
    }

    /**
     * 设置头像
     */
    private void setUserHead() {
        mPhoChooerDialog = new PhotoChooserDialog(getActivity());
        mPhoChooerDialog.showDialog();
        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.setPhoChooerListener(new PhoChooerDialogListener() {
            @Override
            public void firstAlbumReturn(int requestCode, int resultCode, Intent data) {
                /**从相册返回的数据-还未处理**/
                if (data != null) {
                    /**得到图片的全路径**/
                    Uri uri = data.getData();
                    mPhoChooerDialog.crop(uri);
                }
            }

            @Override
            public void firstCameraReturn(int requestCode, int resultCode, Intent data) {
                /**从相机返回的数据-还未处理**/
                if (hasSdcard()) {
                    mPhoChooerDialog.crop(Uri.fromFile(mPhoChooerDialog.getTempFile()
                    ));
                } else {
                    ToastUtil.showToast(mAppContext, "沒有存儲卡,无法存储图片,");
                }
            }

            @Override
            public void processed(int requestCode, int resultCode, Intent data) {
                /**从剪切图片返回的数据-已处理**/
                if (data != null) {
                    Bitmap bitmap = data.getParcelableExtra("data");
                    mIvTitle.setImageBitmap(bitmap);
                }
                try {
                    /** 将临时文件删除**/
                      tempFile.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
