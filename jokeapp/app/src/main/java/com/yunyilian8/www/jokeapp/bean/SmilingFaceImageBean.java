package com.yunyilian8.www.jokeapp.bean;

import android.os.Parcel;
import android.os.Parcelable;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/13  15:31
 * description:
 *****************************************************/
public class SmilingFaceImageBean implements Parcelable {

    /**
     * content : 谁动了我的冰箱！
     * hashId : DDE51B6C09E1557D6542627755901308
     * unixtime : 1418967626
     * updatetime : 2014-12-19 13:40:26
     * url : http://img.juhe.cn/joke/201412/19/DDE51B6C09E1557D6542627755901308.gif
     */

    private String content;
    private String hashId;
    private int unixtime;
    private String updatetime;
    private String url;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public int getUnixtime() {
        return unixtime;
    }

    public void setUnixtime(int unixtime) {
        this.unixtime = unixtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.content);
        dest.writeString(this.hashId);
        dest.writeInt(this.unixtime);
        dest.writeString(this.updatetime);
        dest.writeString(this.url);
    }

    public SmilingFaceImageBean() {
    }

    protected SmilingFaceImageBean(Parcel in) {
        this.content = in.readString();
        this.hashId = in.readString();
        this.unixtime = in.readInt();
        this.updatetime = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<SmilingFaceImageBean> CREATOR = new Parcelable.Creator<SmilingFaceImageBean>() {
        @Override
        public SmilingFaceImageBean createFromParcel(Parcel source) {
            return new SmilingFaceImageBean(source);
        }

        @Override
        public SmilingFaceImageBean[] newArray(int size) {
            return new SmilingFaceImageBean[size];
        }
    };
}
