package com.yunyilian8.www.jokeapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.adapter.SmilingFaceTextAdapter;
import com.yunyilian8.www.jokeapp.base.BaseFragment;
import com.yunyilian8.www.jokeapp.bean.SmilingFaceTextBean;
import com.yunyilian8.www.jokeapp.service.RetrofitService;
import com.yunyilian8.www.jokeapp.utils.JSONUtils;
import com.yunyilian8.www.jokeapp.utils.ToastUtil;
import com.yunyilian8.www.jokeapp.view.kalistview.SmoothListView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Subscriber;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/10  16:29
 * description: 文字笑话fragment
 *****************************************************/
public class SmilingFaceTextFragment extends BaseFragment implements SmoothListView.ISmoothListViewListener {
    //笑话列表
    @InjectView(R.id.listview_receive)
    SmoothListView mListview;

    private View mView;

    private int mPage = 1;
    private int mPageSize = 10;
    private List<SmilingFaceTextBean> mList = new ArrayList<>();
    private SmilingFaceTextAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_smiling_face_text, null);
        ButterKnife.inject(this, mView);

        setListView();

        loadData(false);

        return mView;
    }

    /**
     * 加载数据
     */
    private void loadData(boolean showDialog) {
        loadDataFromNet(new Subscriber<String>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable throwable) {
                ToastUtil.showToast(mAppContext, "获取数据失败！");
            }

            @Override
            public void onNext(String result) {
                if (1 == mPage) {
                    mListview.stopRefresh();
                } else {
                    mListview.stopLoadMore();
                }

                try {
                    if (result != null) {
                        JSONObject jsonObject = JSONUtils.getJSONObject(result);
                        JSONObject str = jsonObject.getJSONObject("result");
                        String dataArray = str.getString("data");
                        setData(dataArray);
                    } else {
                        ToastUtil.showToast(mAppContext, "获取数据失败");
                    }
                } catch (Exception e) {
                    ToastUtil.showToast(mAppContext, "获取数据失败");
                }
            }
        }, RetrofitService.getInstance().getSmilingFaceTextData(mPage, mPageSize));
        if (showDialog)
            showLoadingView();
    }

    private void setListView() {
        //设置listView的上拉和下拉监听
        mListview.setSmoothListViewListener(this);
        mListview.setLoadMoreEnable(true);
        mListview.setRefreshEnable(true);

        mAdapter = new SmilingFaceTextAdapter(getContext());
        mListview.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    /**
     * 重新刷新
     */
    @Override
    public void onRefresh() {
        mPage = 1;
        loadData(false);
    }

    /**
     * 加载更多
     */
    @Override
    public void onLoadMore() {
        mPage++;
        loadData(false);
    }

    /**
     * 设置数据
     *
     * @param data
     */
    public void setData(String data) {

        //判断当前是刷新还是加载更多
        if (1 == mPage) {
            //第一页,就清空list数据
            mList.clear();

            mList = JSONUtils.getList(data, SmilingFaceTextBean.class);
        } else {
            //
            mList.addAll(JSONUtils.getList(data, SmilingFaceTextBean.class));
        }

        mAdapter.setList(mList);
    }
}
