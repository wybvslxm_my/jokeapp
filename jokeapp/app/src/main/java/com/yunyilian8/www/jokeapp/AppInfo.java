package com.yunyilian8.www.jokeapp;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.umeng.analytics.MobclickAgent;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/23  17:39
 * description:
 *****************************************************/
public class AppInfo extends Application {
    @Override public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        //友盟统计设置统计类型
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        //leakCanary注册
   //     LeakCanary.install(this);
    }
}
