package com.yunyilian8.www.jokeapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.adapter.StaticPagerAdapter;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/10  9:52
 * description:
 *****************************************************/
public class TestNomalAdapter extends StaticPagerAdapter {

    private Context mContext;

    private String[] mStrings;

    public  TestNomalAdapter(Context context,String[] s){
        this.mContext = context;

        this.mStrings = s;
    }


    @Override
    public View getView(ViewGroup container, int position) {
        ImageView view = new ImageView(container.getContext());
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        /**加载图片
        ImageLoader.displayImage(mContext, view, uris[position], R.drawable.image_loading);**/

        return view;
    }

    @Override
    public int getCount() {
        return mStrings.length;
    }
}
