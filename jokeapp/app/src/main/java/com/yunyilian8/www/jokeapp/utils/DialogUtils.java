package com.yunyilian8.www.jokeapp.utils;

import android.app.Activity;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnItemClickListener;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/17  15:09
 * description: 弹出dialog工具类
 *****************************************************/
public class DialogUtils {

    private static AlertView mAlertView;


    /**
     * 显示多个按钮list的dialog
     *
     * @param context  activity
     * @param s1       高亮的按钮数组,如不需要传入null
     * @param s2       普通按钮数组
     * @param isBottom 是不是显示在底部  true:显示在底部,false:显示在中间
     * @param listener item点击的监听回调
     */
    public static void showButtonDialog(Activity context, String[] s1, String[] s2, boolean isBottom, final ShowButtonDialogListener listener) {
        AlertView.Style actionSheet;
        if (isBottom) {
            actionSheet = AlertView.Style.ActionSheet;
        } else {
            actionSheet = AlertView.Style.Alert;
        }
        mAlertView = new AlertView(null, null, null, s1, s2,
                context, actionSheet, new OnItemClickListener() {
            @Override
            public void onItemClick(Object o, int position) {
                listener.onItemClick(position);
            }
        });
        mAlertView.setCancelable(true);
        mAlertView.show();
    }

    /**
     * 中间弹出选择dialog 包含标题,内容,确定,取消.
     *
     * @param context  activity
     * @param title    标题
     * @param content  内容
     * @param listener 监听
     */
    public static void showDialog(Activity context, String title, String content, final ShowDialogListener listener) {
        mAlertView = new AlertView(title, content, "取消",
                new String[]{"确定"}, null, context,
                AlertView.Style.Alert, new OnItemClickListener() {
            @Override
            public void onItemClick(Object o, int position) {
                switch (position) {
                    case -1:
                        listener.onDismiss();
                        break;
                    case 0:
                        listener.onFixed();
                        break;
                }

            }
        }).setCancelable(true);

        mAlertView.show();
        mAlertView.setCancelable(false);
    }

    /**
     * 从底部弹出dialog,包括拍照,选择相册,取消
     *
     * @param context  activity
     * @param title    弹出dialog的说明
     * @param listener 回调接口
     */
    public static void bottomDialogShow(final Activity context, String title, final BottomOnItemClickListener listener) {
        mAlertView = new AlertView(title, null, "取消", null,
                new String[]{"拍照", "从相册中选择"},
                context, AlertView.Style.ActionSheet, new OnItemClickListener() {
            public void onItemClick(Object o, int position) {
                listener.onItemClick(o, position);
            }
        });
        mAlertView.setCancelable(true);
        mAlertView.show();
    }


    /**
     * 消息提醒dialog
     *
     * @param context activity
     * @param title   标题
     * @param content 内容
     */
    public static void messageRemindDialog(Activity context, String title, String content) {
        mAlertView = new AlertView(title, content, null, new String[]{"确定"}, null, context,
                AlertView.Style.Alert, null);
        mAlertView.setCancelable(true);
        mAlertView.show();
    }

    /**
     * 关闭dialog
     */
    public static void dissims() {
        if (null != mAlertView){
            mAlertView.dismiss();
        }
    }


    /**
     * 从底部弹出dialog监听
     */
    public interface BottomOnItemClickListener {
        void onItemClick(Object o, int position);
    }

    /**
     * 中间选择dialog监听
     */
    public interface ShowDialogListener {
        //取消
        void onFixed();

        //确定
        void onDismiss();
    }

    /**
     * 中间弹出多按钮dialog监听
     */
    public interface ShowButtonDialogListener {
        void onItemClick(int position);
    }
}
