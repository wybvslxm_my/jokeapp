package com.yunyilian8.www.jokeapp.service;
/**
 *
 */

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;
import rx.android.BuildConfig;


public class RetrofitService {

    public static final int PAGE_SIZE = 10;

    // 需求/发布 测试
    public final static String ServerRootPath = "http://japi.juhe.cn/";

    //笑话接口key
    public final static String myKey = "929407301ee3a8a420507fe763111f99";
    //历史上的今天对应key
    public final static String dateHistoricalKey = "2b9c4d5baf33487766fdefc0813cfe5c";

    private static RetrofitService retrofitService = new RetrofitService();
    private static RetrofitInterface retrofitInterface;

    private RetrofitService() {
        initRetrofit();
    }

    public static RetrofitService getInstance() {
        if (retrofitService == null) {
            retrofitService = new RetrofitService();
        }
        return retrofitService;
    }

    private void initRetrofit() {
        OkHttpClient httpClient;
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(logging).build();
        } else {
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .build();
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServerRootPath)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }

    public static void changeServerURL(String url) {
        OkHttpClient httpClient;
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(logging).build();
        } else {
            httpClient = new OkHttpClient.Builder()
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .build();
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(httpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
    }


    /**
     * 获取文本笑话
     **/
    public Observable<String> getSmilingFaceTextData(int page, int pageSize) {
        return retrofitInterface.getSmilingFaceTextData(myKey, page, pageSize);
    }

    /**
     * 获取图片笑话
     **/
    public Observable<String> getSmilingFaceImageData(int page, int pageSize) {
        return retrofitInterface.getSmilingFaceImageData(myKey, page, pageSize);
    }

    /**
     * 历史上的今天
     **/
    public Observable<String> queryEvent(String date) {
        return retrofitInterface.queryEvent(date,dateHistoricalKey);
    }
}
