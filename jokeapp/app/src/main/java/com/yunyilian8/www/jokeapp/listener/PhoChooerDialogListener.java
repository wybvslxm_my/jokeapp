package com.yunyilian8.www.jokeapp.listener;

import android.content.Intent;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/24  17:53
 * description: 头像选择dialog对应listener
 *****************************************************/
public interface PhoChooerDialogListener {
    //相册返回,无处理
    void firstAlbumReturn(int requestCode, int resultCode, Intent data);

    //相机返回,无处理
    void firstCameraReturn(int requestCode, int resultCode, Intent data);

    //已经处理
    void processed(int requestCode, int resultCode, Intent data);
}
