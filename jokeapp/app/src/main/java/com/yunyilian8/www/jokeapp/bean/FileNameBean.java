package com.yunyilian8.www.jokeapp.bean;

import java.io.Serializable;
import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/23  12:00
 * description: 播放源数据封装bean
 *****************************************************/
public class FileNameBean implements Serializable {
    private String name;
    private String path;
    private boolean unfolded;
    private boolean isAssetsFile;

    public boolean isAssetsFile() {
        return isAssetsFile;
    }

    public void setAssetsFile(boolean assetsFile) {
        isAssetsFile = assetsFile;
    }

    private List<VideoFileNameBean> videoFileNameBeenList;

    public List<VideoFileNameBean> getVideoFileNameBeenList() {
        return videoFileNameBeenList;
    }

    public void setVideoFileNameBeenList(List<VideoFileNameBean> videoFileNameBeenList) {
        this.videoFileNameBeenList = videoFileNameBeenList;
    }

    public String getName() {
        return name;
    }

    public void setName(String videoName) {
        this.name = videoName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String url) {
        this.path = url;
    }

    public boolean isUnfolded() {
        return unfolded;
    }

    public void setUnfolded(boolean unfolded) {
        this.unfolded = unfolded;
    }

}
