package com.yunyilian8.www.jokeapp.bean;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/3/3  10:33
 * description: 历史上的今天 listView封装bean对象
 *****************************************************/
public class DateHistoricalListBean {

    /**
     * day : 2/1
     * date : 1587年02月01日
     * title : 英国伊丽莎白女王一世签署命令，处死苏格兰的玛丽女王
     * e_id : 1462
     */

    private String day;
    private String date;
    private String title;
    private String e_id;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getE_id() {
        return e_id;
    }

    public void setE_id(String e_id) {
        this.e_id = e_id;
    }
}
