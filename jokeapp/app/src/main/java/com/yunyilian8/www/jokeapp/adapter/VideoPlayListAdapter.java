package com.yunyilian8.www.jokeapp.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextPaint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yunyilian8.www.jokeapp.R;
import com.yunyilian8.www.jokeapp.base.SimpleBaseAdapter;
import com.yunyilian8.www.jokeapp.bean.FileNameBean;
import com.yunyilian8.www.jokeapp.bean.VideoFileNameBean;

import java.util.List;

/*****************************************************
 * author:      wyb
 * email:       276698048@qq.com
 * date:        2017/2/21  14:32
 * description: video list列表对应adapter
 *****************************************************/
public class VideoPlayListAdapter extends SimpleBaseAdapter<String> {
    private List<VideoFileNameBean> mVideoFileNameList;
    private List<FileNameBean> mFileNameList;
    private int mType = -1;
    private VideoListListener mListener;

    public VideoPlayListAdapter(Context context, Object list, int type) {
        super(context);
        switch (type) {
            case 2:
                mVideoFileNameList = (List<VideoFileNameBean>) list;
                break;
            case 1:
                mFileNameList = (List<FileNameBean>) list;
                break;
        }
        this.mType = type;
    }

    public VideoPlayListAdapter(Context context, List list) {
        super(context, list);
    }

    @Override
    public int getCount() {
        switch (mType) {
            case 2:
                return mVideoFileNameList == null ? 0 : mVideoFileNameList.size();

            case 1:
                return mFileNameList == null ? 0 : mFileNameList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        switch (mType) {
            case 2:
                return mVideoFileNameList == null ? 0 : mVideoFileNameList.get(position);
            case 1:
                return mFileNameList == null ? 0 : mFileNameList.get(position);
        }
        return 0;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        MyHolder holder;
        if (null == convertView) {
            holder = new MyHolder();
            convertView = View.inflate(mContext, R.layout.item_play_video_list, null);
            holder.rl = (RelativeLayout) convertView.findViewById(R.id.rl_layout);
            holder.tv = (TextView) convertView.findViewById(R.id.tv);
            holder.iv = (ImageView) convertView.findViewById(R.id.iv);

            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        switch (mType) {
            case 1:

                FileNameBean fileNameBean = mFileNameList.get(position);
                //根据子view的对象信息,设置该item的背景状态以及文字颜色
                if (fileNameBean.isUnfolded()) {
                    holder.tv.setBackgroundColor(mContext.getResources().getColor(R.color.cornflowerblue));
                    holder.tv.setTextColor(mContext.getResources().getColor(R.color.white));
                } else {
                    holder.tv.setBackground(mContext.getResources().getDrawable(R.drawable.selector_bg_blue));
                    holder.tv.setTextColor(mContext.getResources().getColor(R.color.gray15));
                }

                holder.tv.setText(fileNameBean.getName());
                holder.iv.setVisibility(View.GONE);
                TextPaint tp = holder.tv.getPaint();
                tp.setFakeBoldText(true);
                //item长按点击事件
                holder.tv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mListener.longClick(position,mType,mFileNameList.get(position).getName(),mFileNameList.get(position).getPath());
                        return true;
                    }
                });

                break;
            case 2:
                final VideoFileNameBean videoFileNameBean = mVideoFileNameList.get(position);

                holder.iv.setVisibility(View.VISIBLE);
                //根据子view的对象信息,设置该item的背景状态以及文字颜色
                if (videoFileNameBean.isUnfolded()) {
                    holder.tv.setBackgroundColor(mContext.getResources().getColor(R.color.cornflowerblue));
                    holder.tv.setTextColor(mContext.getResources().getColor(R.color.white));
                } else {
                    holder.tv.setBackground(mContext.getResources().getDrawable(R.drawable.selector_bg_blue));
                    holder.tv.setTextColor(mContext.getResources().getColor(R.color.gray15));
                }

                if (videoFileNameBean.isCollect()){
                    //是被收藏的
                    holder.iv.setBackgroundResource(R.mipmap.icon_collect_check);
                }else {
                    //没有收藏的
                    holder.iv.setBackgroundResource(R.mipmap.icon_collect_noo);
                }

                holder.tv.setText(videoFileNameBean.getName());
                holder.tv.setTextSize(12);
                //长按监听
                holder.tv.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mListener.longClick(position,mType,videoFileNameBean.getName(),videoFileNameBean.getURL());
                        return true;
                    }
                });
                break;
        }

        //设置item的点击监听
        holder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mType) {
                    case 2:
                        mListener.chooseItem(position, 2, mVideoFileNameList.get(position).getName(), mVideoFileNameList.get(position).getURL());
                        break;
                    case 1:
                        mListener.chooseItem(position, 1, mFileNameList.get(position).getName(),mFileNameList.get(position).getPath());
                        break;
                }
            }
        });

        return convertView;
    }

    class MyHolder {
        RelativeLayout rl;
        TextView tv;
        ImageView iv;
    }

    public void setListener(VideoListListener listener) {
        this.mListener = listener;
    }

    public interface VideoListListener {
        void chooseItem(int position, int type, String programFileName, String url);
        void longClick(int position,int type,String programFileName,String url);
    }
}
