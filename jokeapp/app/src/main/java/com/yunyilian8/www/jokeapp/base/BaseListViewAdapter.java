package com.yunyilian8.www.jokeapp.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * listView Adapter的抽取
 * @param <T> 具体的数据类型
 */
public abstract class BaseListViewAdapter<T> extends BaseAdapter {

    public Context mContext;
    public List<T> mList;

    public BaseListViewAdapter(Context context, List<T> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setList(List<T> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == mList ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null == mList ? null : mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = baseGetView(position, convertView, parent);
        return view;
    }

    protected abstract View baseGetView(int position, View convertView, ViewGroup parent);
}
