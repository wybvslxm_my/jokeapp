package com.yunyilian8.www.jokeapp.utils;


import com.yunyilian8.www.jokeapp.bean.AnyEventType;

import de.greenrobot.event.EventBus;

/**
 * EventBus通知事件常量
 * Created by Andy on 2016/4/20.
 */
public class EventBusEvents {

    public static void EventBusChange(AnyEventType e) {
        EventBus.getDefault().post(e);
    }

    //视频播放-停止
    public static final int EVENT_VIDEO_STOP = 10001;
    //视频播放-开始
    public static final int EVENT_VIDEO_START = 10002;
    //夜间模式-开启
    public static final int NIGHT_MODE_ON = 10003;
    //夜间模式-关闭
    public static final int NIGHT_MODE_OFF = 10004;
    //刷新节目列表
    public static final int REFRESH_PROGRAM_LIST = 10005;

}
